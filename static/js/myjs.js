$(document).ready(function(){
    $('#button1').click(function(){
        $('.block2').css("background-color", "rgb(24, 73, 138)");
        $('.block').css("background-color", "rgb(123, 180, 255)");
        $('.span1').css("background-color", "rgb(4, 29, 63)");
        $('body').css("background-color", "rgb(62, 130, 219)");
        $('#submit').attr('class', 'btn btn-primary');
        $('#mainNav').attr('class', 'navbar navbar-expand-lg navbar-dark bg-primary');
        $('#button1').css("border", "2px solid white");
        $('#button2').css("border", "2px solid black");
        $('#button3').css("border", "2px solid black");
        $('th').css("background-color", "rgb(62, 130, 219)");
        $('td').css("background-color", "rgb(123, 180, 255)");
    });
});
$(document).ready(function(){
    $('#button2').click(function(){
        $('.block2').css("background-color", "rgb(47, 138, 24)");
        $('.block').css("background-color", "rgb(204, 255, 123)");
        $('.span1').css("background-color", "rgb(31, 63, 4)");
        $('body').css("background-color", "rgb(151, 219, 62)");
        $('#submit').attr('class', 'btn btn-success');
        $('#mainNav').attr('class', 'navbar navbar-expand-lg navbar-dark bg-success');
        $('#button1').css("border", "2px solid black");
        $('#button2').css("border", "2px solid white");
        $('#button3').css("border", "2px solid black");
        $('th').css("background-color", "rgb(151, 219, 62)");
        $('td').css("background-color", "rgb(204, 255, 123)");
    });
});
$(document).ready(function(){
    $('#button3').click(function(){
        $('.block2').css("background-color", "rgb(138, 24, 24)");
        $('.block').css("background-color", "rgb(255, 180, 123)");
        $('.span1').css("background-color", "rgb(63, 4, 4)");
        $('body').css("background-color", "rgb(219, 130, 62)");
        $('#submit').attr('class', 'btn btn-danger');
        $('#mainNav').attr('class', 'navbar navbar-expand-lg navbar-dark bg-danger');
        $('#button1').css("border", "2px solid black");
        $('#button2').css("border", "2px solid black");
        $('#button3').css("border", "2px solid white");
        $('th').css("background-color", "rgb(219, 130, 62)");
        $('td').css("background-color", "rgb(255, 180, 123)");
    });
});
$(document).ready(function(){
    $('#numb1').click(function(){
        $("#txt1").toggle();
    });
});
$(document).ready(function(){
    $('#numb2').click(function(){
        $("#txt2").toggle();
    });
});
$(document).ready(function(){
    $('#numb3').click(function(){
        $("#txt3").toggle();
    });
});
$(document).ready(function(){
    $('#numb4').click(function(){
        $("#txt4").toggle();
    });
});
$(document).ready(function(){
    $('#dw1').click(function(){
        var txt = $('#txt2').text();
        $('#txt2').text($('#txt1').text());
        $('#txt1').text(txt);
        var tit = $('#numb2').html();
        $('#numb2').html($('#numb1').html());
        $('#numb1').html(tit);
    });
});
$(document).ready(function(){
    $('#dw2').click(function(){
        var txt = $('#txt3').text();
        $('#txt3').text($('#txt2').text());
        $('#txt2').text(txt);
        var tit = $('#numb3').html();
        $('#numb3').html($('#numb2').html());
        $('#numb2').html(tit);
    });
});
$(document).ready(function(){
    $('#dw3').click(function(){
        var txt = $('#txt4').text();
        $('#txt4').text($('#txt3').text());
        $('#txt3').text(txt);
        var tit = $('#numb4').html();
        $('#numb4').html($('#numb3').html());
        $('#numb3').html(tit);
    });
});
$(document).ready(function(){
    $('#up2').click(function(){
        var txt = $('#txt1').text();
        $('#txt1').text($('#txt2').text());
        $('#txt2').text(txt);
        var tit = $('#numb1').html();
        $('#numb1').html($('#numb2').html());
        $('#numb2').html(tit);
    });
});
$(document).ready(function(){
    $('#up3').click(function(){
        var txt = $('#txt2').text();
        $('#txt2').text($('#txt3').text());
        $('#txt3').text(txt);
        var tit = $('#numb2').html();
        $('#numb2').html($('#numb3').html());
        $('#numb3').html(tit);
    });
});
$(document).ready(function(){
    $('#up4').click(function(){
        var txt = $('#txt3').text();
        $('#txt3').text($('#txt4').text());
        $('#txt4').text(txt);
        var tit = $('#numb3').html();
        $('#numb3').html($('#numb4').html());
        $('#numb4').html(tit);
    });
});