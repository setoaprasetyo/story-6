window.onload = function() {
    var place = document.getElementById("booktable");
    var btn = document.getElementById("searchbutton");
    var sort = [0,0,0,0,0,0,0,0,0,0];

    btn.addEventListener("click",function(){
        var value = document.getElementById("searchsubmit").value;
        var ourRequest = new XMLHttpRequest();
        ourRequest.open('GET','https://www.googleapis.com/books/v1/volumes?q=' + value);
        ourRequest.onload = function() {
            var ourdata = JSON.parse(ourRequest.responseText);
            renderHTML(ourdata);
        };
        ourRequest.send();
    })

    function Author(aut) {
        var boop = "";
        for (var ii = 0; ii < aut.length; ii++) {
            if (ii==0) {
                boop += aut[ii];
            } else {
                boop += ", " + aut[ii];
            }
        }
        return boop
    }

    function renderHTML(data) {
        var HTMLstring ='<thead><tr><th style="width:10%">Image</th>'+
        '<th style="width:10%">Title</th><th style="width:7%">Author</th>'+
        '<th style="width:6%">Publisher</th>'+
        '<th style="width:5%">Date</th>'+
        '<th style="width:55%">Description</th>'+
        '<th style="width:7%">Like</th></tr></thead>' +
        '<tbody>';
        for (var i = 0; i < 10; i++) {
           const image = data.items[i].volumeInfo.imageLinks.smallThumbnail
           const title = data.items[i].volumeInfo.title
           const author = Author(data.items[i].volumeInfo.authors)
           const publisher = data.items[i].volumeInfo.publisher
           const date = data.items[i].volumeInfo.publishedDate
           const desc = data.items[i].volumeInfo.description

           HTMLstring += "<tr id='row" + i + "'>" +
           "<td><image src='" + image + "'></td>" +
           "<td id='booktit" + i + "'>" + title + "</td>" +
           "<td>" + author + "</td>" +
           "<td>" + publisher + "</td>" +
           "<td>" + date + "</td>" +
           "<td>" + desc + "</td>" +
           "<td><button class='hdnbtn' id='btn" + i + "'><b style='font-size:30px; color: white;'>&hearts;</b></button><p id='like" + i + "' class='hidden'>0</p></td></tr>";
        }
        HTMLstring += '</tbody>'
        place.innerHTML = HTMLstring;
    }
}
$(document).ready(function(){
    $('#booktable').on('click','#btn0',function() {
        var num = parseInt($('#booktable').find('#like0').text()) + 1;
        $('#booktable').find('#like0').text(''+num+'');
    })
})
$(document).ready(function(){
    $('#booktable').on('click','#btn1',function() {
        var num = parseInt($('#booktable').find('#like1').text()) + 1;
        $('#booktable').find('#like1').text(''+num+'');
    })
})
$(document).ready(function(){
    $('#booktable').on('click','#btn2',function() {
        var num = parseInt($('#booktable').find('#like2').text()) + 1;
        $('#booktable').find('#like2').text(''+num+'');
    })
})
$(document).ready(function(){
    $('#booktable').on('click','#btn3',function() {
        var num = parseInt($('#booktable').find('#like3').text()) + 1;
        $('#booktable').find('#like3').text(''+num+'');
    })
})
$(document).ready(function(){
    $('#booktable').on('click','#btn4',function() {
        var num = parseInt($('#booktable').find('#like4').text()) + 1;
        $('#booktable').find('#like4').text(''+num+'');
    })
})
$(document).ready(function(){
    $('#booktable').on('click','#btn5',function() {
        var num = parseInt($('#booktable').find('#like5').text()) + 1;
        $('#booktable').find('#like5').text(''+num+'');
    })
})
$(document).ready(function(){
    $('#booktable').on('click','#btn6',function() {
        var num = parseInt($('#booktable').find('#like6').text()) + 1;
        $('#booktable').find('#like6').text(''+num+'');
    })
})
$(document).ready(function(){
    $('#booktable').on('click','#btn7',function() {
        var num = parseInt($('#booktable').find('#like7').text()) + 1;
        $('#booktable').find('#like7').text(''+num+'');
    })
})
$(document).ready(function(){
    $('#booktable').on('click','#btn8',function() {
        var num = parseInt($('#booktable').find('#like8').text()) + 1;
        $('#booktable').find('#like8').text(''+num+'');
    })
})
$(document).ready(function(){
    $('#booktable').on('click','#btn9',function() {
        var num = parseInt($('#booktable').find('#like9').text()) + 1;
        $('#booktable').find('#like9').text(''+num+'');
    })
})
$(document).ready(function(){
    function sorter () {
        var sort = [
            parseInt($('#booktable').find('#like0').text()),
            parseInt($('#booktable').find('#like1').text()),
            parseInt($('#booktable').find('#like2').text()),
            parseInt($('#booktable').find('#like3').text()),
            parseInt($('#booktable').find('#like4').text()),
            parseInt($('#booktable').find('#like5').text()),
            parseInt($('#booktable').find('#like6').text()),
            parseInt($('#booktable').find('#like7').text()),
            parseInt($('#booktable').find('#like8').text()),
            parseInt($('#booktable').find('#like9').text())
        ];
        var btits = [
            $('#booktable').find('#booktit0').text(),
            $('#booktable').find('#booktit1').text(),
            $('#booktable').find('#booktit2').text(),
            $('#booktable').find('#booktit3').text(),
            $('#booktable').find('#booktit4').text(),
            $('#booktable').find('#booktit5').text(),
            $('#booktable').find('#booktit6').text(),
            $('#booktable').find('#booktit7').text(),
            $('#booktable').find('#booktit8').text(),
            $('#booktable').find('#booktit9').text(),
        ];
        for (var i = 0; i < 10; i++) {
            for (var j = i + 1; j < 10; j++){
                if (sort[i] > sort[j]) {
                    var temp = sort[i];
                    var temp2 = btits[i];
                    sort[i] = sort[j];
                    btits[i] = btits[j];
                    sort[j] = temp;
                    btits[j] = temp2;
                }
            }
        }
        var modtxt = ''
        for (var ii = 9; ii > 4; ii--) {
            modtxt += '<p>' + btits[ii] + ' with ' + sort[ii] + ' likes</p>';
        }
        $('#inmodal').text(modtxt);
    }
    $('#searchbutton').click(function() {
        var sort = [
            parseInt($('#booktable').find('#like0').text()),
            parseInt($('#booktable').find('#like1').text()),
            parseInt($('#booktable').find('#like2').text()),
            parseInt($('#booktable').find('#like3').text()),
            parseInt($('#booktable').find('#like4').text()),
            parseInt($('#booktable').find('#like5').text()),
            parseInt($('#booktable').find('#like6').text()),
            parseInt($('#booktable').find('#like7').text()),
            parseInt($('#booktable').find('#like8').text()),
            parseInt($('#booktable').find('#like9').text())
        ];
        var btits = [
            $('#booktable').find('#booktit0').text(),
            $('#booktable').find('#booktit1').text(),
            $('#booktable').find('#booktit2').text(),
            $('#booktable').find('#booktit3').text(),
            $('#booktable').find('#booktit4').text(),
            $('#booktable').find('#booktit5').text(),
            $('#booktable').find('#booktit6').text(),
            $('#booktable').find('#booktit7').text(),
            $('#booktable').find('#booktit8').text(),
            $('#booktable').find('#booktit9').text(),
        ];
        for (var i = 0; i < 10; i++) {
            for (var j = i + 1; j < 10; j++){
                if (sort[i] > sort[j]) {
                    var temp = sort[i];
                    var temp2 = btits[i];
                    sort[i] = sort[j];
                    btits[i] = btits[j];
                    sort[j] = temp;
                    btits[j] = temp2;
                }
            }
        }
        var modtxt = ''
        for (var ii = 9; ii > 4; ii--) {
            modtxt += '<p><b>' + btits[ii] + '</b>: ' + sort[ii] + ' likes</p>';
        }
        $('#inmodal').html(modtxt);
    });
    $('#booktable').on('click','#btn0',function() {
        var sort = [
            parseInt($('#booktable').find('#like0').text()),
            parseInt($('#booktable').find('#like1').text()),
            parseInt($('#booktable').find('#like2').text()),
            parseInt($('#booktable').find('#like3').text()),
            parseInt($('#booktable').find('#like4').text()),
            parseInt($('#booktable').find('#like5').text()),
            parseInt($('#booktable').find('#like6').text()),
            parseInt($('#booktable').find('#like7').text()),
            parseInt($('#booktable').find('#like8').text()),
            parseInt($('#booktable').find('#like9').text())
        ];
        var btits = [
            $('#booktable').find('#booktit0').text(),
            $('#booktable').find('#booktit1').text(),
            $('#booktable').find('#booktit2').text(),
            $('#booktable').find('#booktit3').text(),
            $('#booktable').find('#booktit4').text(),
            $('#booktable').find('#booktit5').text(),
            $('#booktable').find('#booktit6').text(),
            $('#booktable').find('#booktit7').text(),
            $('#booktable').find('#booktit8').text(),
            $('#booktable').find('#booktit9').text(),
        ];
        for (var i = 0; i < 10; i++) {
            for (var j = i + 1; j < 10; j++){
                if (sort[i] > sort[j]) {
                    var temp = sort[i];
                    var temp2 = btits[i];
                    sort[i] = sort[j];
                    btits[i] = btits[j];
                    sort[j] = temp;
                    btits[j] = temp2;
                }
            }
        }
        var modtxt = ''
        for (var ii = 9; ii > 4; ii--) {
            modtxt += '<p><b>' + btits[ii] + '</b>: ' + sort[ii] + ' likes</p>';
        }
        $('#inmodal').html(modtxt);
    });
    $('#booktable').on('click','#btn1',function() {
        var sort = [
            parseInt($('#booktable').find('#like0').text()),
            parseInt($('#booktable').find('#like1').text()),
            parseInt($('#booktable').find('#like2').text()),
            parseInt($('#booktable').find('#like3').text()),
            parseInt($('#booktable').find('#like4').text()),
            parseInt($('#booktable').find('#like5').text()),
            parseInt($('#booktable').find('#like6').text()),
            parseInt($('#booktable').find('#like7').text()),
            parseInt($('#booktable').find('#like8').text()),
            parseInt($('#booktable').find('#like9').text())
        ];
        var btits = [
            $('#booktable').find('#booktit0').text(),
            $('#booktable').find('#booktit1').text(),
            $('#booktable').find('#booktit2').text(),
            $('#booktable').find('#booktit3').text(),
            $('#booktable').find('#booktit4').text(),
            $('#booktable').find('#booktit5').text(),
            $('#booktable').find('#booktit6').text(),
            $('#booktable').find('#booktit7').text(),
            $('#booktable').find('#booktit8').text(),
            $('#booktable').find('#booktit9').text(),
        ];
        for (var i = 0; i < 10; i++) {
            for (var j = i + 1; j < 10; j++){
                if (sort[i] > sort[j]) {
                    var temp = sort[i];
                    var temp2 = btits[i];
                    sort[i] = sort[j];
                    btits[i] = btits[j];
                    sort[j] = temp;
                    btits[j] = temp2;
                }
            }
        }
        var modtxt = ''
        for (var ii = 9; ii > 4; ii--) {
            modtxt += '<p><b>' + btits[ii] + '</b>: ' + sort[ii] + ' likes</p>';
        }
        $('#inmodal').html(modtxt);
    });
    $('#booktable').on('click','#btn2',function() {
        var sort = [
            parseInt($('#booktable').find('#like0').text()),
            parseInt($('#booktable').find('#like1').text()),
            parseInt($('#booktable').find('#like2').text()),
            parseInt($('#booktable').find('#like3').text()),
            parseInt($('#booktable').find('#like4').text()),
            parseInt($('#booktable').find('#like5').text()),
            parseInt($('#booktable').find('#like6').text()),
            parseInt($('#booktable').find('#like7').text()),
            parseInt($('#booktable').find('#like8').text()),
            parseInt($('#booktable').find('#like9').text())
        ];
        var btits = [
            $('#booktable').find('#booktit0').text(),
            $('#booktable').find('#booktit1').text(),
            $('#booktable').find('#booktit2').text(),
            $('#booktable').find('#booktit3').text(),
            $('#booktable').find('#booktit4').text(),
            $('#booktable').find('#booktit5').text(),
            $('#booktable').find('#booktit6').text(),
            $('#booktable').find('#booktit7').text(),
            $('#booktable').find('#booktit8').text(),
            $('#booktable').find('#booktit9').text(),
        ];
        for (var i = 0; i < 10; i++) {
            for (var j = i + 1; j < 10; j++){
                if (sort[i] > sort[j]) {
                    var temp = sort[i];
                    var temp2 = btits[i];
                    sort[i] = sort[j];
                    btits[i] = btits[j];
                    sort[j] = temp;
                    btits[j] = temp2;
                }
            }
        }
        var modtxt = ''
        for (var ii = 9; ii > 4; ii--) {
            modtxt += '<p><b>' + btits[ii] + '</b>: ' + sort[ii] + ' likes</p>';
        }
        $('#inmodal').html(modtxt);
    });
    $('#booktable').on('click','#btn3',function() {
        var sort = [
            parseInt($('#booktable').find('#like0').text()),
            parseInt($('#booktable').find('#like1').text()),
            parseInt($('#booktable').find('#like2').text()),
            parseInt($('#booktable').find('#like3').text()),
            parseInt($('#booktable').find('#like4').text()),
            parseInt($('#booktable').find('#like5').text()),
            parseInt($('#booktable').find('#like6').text()),
            parseInt($('#booktable').find('#like7').text()),
            parseInt($('#booktable').find('#like8').text()),
            parseInt($('#booktable').find('#like9').text())
        ];
        var btits = [
            $('#booktable').find('#booktit0').text(),
            $('#booktable').find('#booktit1').text(),
            $('#booktable').find('#booktit2').text(),
            $('#booktable').find('#booktit3').text(),
            $('#booktable').find('#booktit4').text(),
            $('#booktable').find('#booktit5').text(),
            $('#booktable').find('#booktit6').text(),
            $('#booktable').find('#booktit7').text(),
            $('#booktable').find('#booktit8').text(),
            $('#booktable').find('#booktit9').text(),
        ];
        for (var i = 0; i < 10; i++) {
            for (var j = i + 1; j < 10; j++){
                if (sort[i] > sort[j]) {
                    var temp = sort[i];
                    var temp2 = btits[i];
                    sort[i] = sort[j];
                    btits[i] = btits[j];
                    sort[j] = temp;
                    btits[j] = temp2;
                }
            }
        }
        var modtxt = ''
        for (var ii = 9; ii > 4; ii--) {
            modtxt += '<p><b>' + btits[ii] + '</b>: ' + sort[ii] + ' likes</p>';
        }
        $('#inmodal').html(modtxt);
    });
    $('#booktable').on('click','#btn4',function() {
        var sort = [
            parseInt($('#booktable').find('#like0').text()),
            parseInt($('#booktable').find('#like1').text()),
            parseInt($('#booktable').find('#like2').text()),
            parseInt($('#booktable').find('#like3').text()),
            parseInt($('#booktable').find('#like4').text()),
            parseInt($('#booktable').find('#like5').text()),
            parseInt($('#booktable').find('#like6').text()),
            parseInt($('#booktable').find('#like7').text()),
            parseInt($('#booktable').find('#like8').text()),
            parseInt($('#booktable').find('#like9').text())
        ];
        var btits = [
            $('#booktable').find('#booktit0').text(),
            $('#booktable').find('#booktit1').text(),
            $('#booktable').find('#booktit2').text(),
            $('#booktable').find('#booktit3').text(),
            $('#booktable').find('#booktit4').text(),
            $('#booktable').find('#booktit5').text(),
            $('#booktable').find('#booktit6').text(),
            $('#booktable').find('#booktit7').text(),
            $('#booktable').find('#booktit8').text(),
            $('#booktable').find('#booktit9').text(),
        ];
        for (var i = 0; i < 10; i++) {
            for (var j = i + 1; j < 10; j++){
                if (sort[i] > sort[j]) {
                    var temp = sort[i];
                    var temp2 = btits[i];
                    sort[i] = sort[j];
                    btits[i] = btits[j];
                    sort[j] = temp;
                    btits[j] = temp2;
                }
            }
        }
        var modtxt = ''
        for (var ii = 9; ii > 4; ii--) {
            modtxt += '<p><b>' + btits[ii] + '</b>: ' + sort[ii] + ' likes</p>';
        }
        $('#inmodal').html(modtxt);
    });
    $('#booktable').on('click','#btn5',function() {
        var sort = [
            parseInt($('#booktable').find('#like0').text()),
            parseInt($('#booktable').find('#like1').text()),
            parseInt($('#booktable').find('#like2').text()),
            parseInt($('#booktable').find('#like3').text()),
            parseInt($('#booktable').find('#like4').text()),
            parseInt($('#booktable').find('#like5').text()),
            parseInt($('#booktable').find('#like6').text()),
            parseInt($('#booktable').find('#like7').text()),
            parseInt($('#booktable').find('#like8').text()),
            parseInt($('#booktable').find('#like9').text())
        ];
        var btits = [
            $('#booktable').find('#booktit0').text(),
            $('#booktable').find('#booktit1').text(),
            $('#booktable').find('#booktit2').text(),
            $('#booktable').find('#booktit3').text(),
            $('#booktable').find('#booktit4').text(),
            $('#booktable').find('#booktit5').text(),
            $('#booktable').find('#booktit6').text(),
            $('#booktable').find('#booktit7').text(),
            $('#booktable').find('#booktit8').text(),
            $('#booktable').find('#booktit9').text(),
        ];
        for (var i = 0; i < 10; i++) {
            for (var j = i + 1; j < 10; j++){
                if (sort[i] > sort[j]) {
                    var temp = sort[i];
                    var temp2 = btits[i];
                    sort[i] = sort[j];
                    btits[i] = btits[j];
                    sort[j] = temp;
                    btits[j] = temp2;
                }
            }
        }
        var modtxt = ''
        for (var ii = 9; ii > 4; ii--) {
            modtxt += '<p><b>' + btits[ii] + '</b>: ' + sort[ii] + ' likes</p>';
        }
        $('#inmodal').html(modtxt);
    });
    $('#booktable').on('click','#btn6',function() {
        var sort = [
            parseInt($('#booktable').find('#like0').text()),
            parseInt($('#booktable').find('#like1').text()),
            parseInt($('#booktable').find('#like2').text()),
            parseInt($('#booktable').find('#like3').text()),
            parseInt($('#booktable').find('#like4').text()),
            parseInt($('#booktable').find('#like5').text()),
            parseInt($('#booktable').find('#like6').text()),
            parseInt($('#booktable').find('#like7').text()),
            parseInt($('#booktable').find('#like8').text()),
            parseInt($('#booktable').find('#like9').text())
        ];
        var btits = [
            $('#booktable').find('#booktit0').text(),
            $('#booktable').find('#booktit1').text(),
            $('#booktable').find('#booktit2').text(),
            $('#booktable').find('#booktit3').text(),
            $('#booktable').find('#booktit4').text(),
            $('#booktable').find('#booktit5').text(),
            $('#booktable').find('#booktit6').text(),
            $('#booktable').find('#booktit7').text(),
            $('#booktable').find('#booktit8').text(),
            $('#booktable').find('#booktit9').text(),
        ];
        for (var i = 0; i < 10; i++) {
            for (var j = i + 1; j < 10; j++){
                if (sort[i] > sort[j]) {
                    var temp = sort[i];
                    var temp2 = btits[i];
                    sort[i] = sort[j];
                    btits[i] = btits[j];
                    sort[j] = temp;
                    btits[j] = temp2;
                }
            }
        }
        var modtxt = ''
        for (var ii = 9; ii > 4; ii--) {
            modtxt += '<p><b>' + btits[ii] + '</b>: ' + sort[ii] + ' likes</p>';
        }
        $('#inmodal').html(modtxt);
    });
    $('#booktable').on('click','#btn7',function() {
        var sort = [
            parseInt($('#booktable').find('#like0').text()),
            parseInt($('#booktable').find('#like1').text()),
            parseInt($('#booktable').find('#like2').text()),
            parseInt($('#booktable').find('#like3').text()),
            parseInt($('#booktable').find('#like4').text()),
            parseInt($('#booktable').find('#like5').text()),
            parseInt($('#booktable').find('#like6').text()),
            parseInt($('#booktable').find('#like7').text()),
            parseInt($('#booktable').find('#like8').text()),
            parseInt($('#booktable').find('#like9').text())
        ];
        var btits = [
            $('#booktable').find('#booktit0').text(),
            $('#booktable').find('#booktit1').text(),
            $('#booktable').find('#booktit2').text(),
            $('#booktable').find('#booktit3').text(),
            $('#booktable').find('#booktit4').text(),
            $('#booktable').find('#booktit5').text(),
            $('#booktable').find('#booktit6').text(),
            $('#booktable').find('#booktit7').text(),
            $('#booktable').find('#booktit8').text(),
            $('#booktable').find('#booktit9').text(),
        ];
        for (var i = 0; i < 10; i++) {
            for (var j = i + 1; j < 10; j++){
                if (sort[i] > sort[j]) {
                    var temp = sort[i];
                    var temp2 = btits[i];
                    sort[i] = sort[j];
                    btits[i] = btits[j];
                    sort[j] = temp;
                    btits[j] = temp2;
                }
            }
        }
        var modtxt = ''
        for (var ii = 9; ii > 4; ii--) {
            modtxt += '<p><b>' + btits[ii] + '</b>: ' + sort[ii] + ' likes</p>';
        }
        $('#inmodal').html(modtxt);
    });
    $('#booktable').on('click','#btn8',function() {
        var sort = [
            parseInt($('#booktable').find('#like0').text()),
            parseInt($('#booktable').find('#like1').text()),
            parseInt($('#booktable').find('#like2').text()),
            parseInt($('#booktable').find('#like3').text()),
            parseInt($('#booktable').find('#like4').text()),
            parseInt($('#booktable').find('#like5').text()),
            parseInt($('#booktable').find('#like6').text()),
            parseInt($('#booktable').find('#like7').text()),
            parseInt($('#booktable').find('#like8').text()),
            parseInt($('#booktable').find('#like9').text())
        ];
        var btits = [
            $('#booktable').find('#booktit0').text(),
            $('#booktable').find('#booktit1').text(),
            $('#booktable').find('#booktit2').text(),
            $('#booktable').find('#booktit3').text(),
            $('#booktable').find('#booktit4').text(),
            $('#booktable').find('#booktit5').text(),
            $('#booktable').find('#booktit6').text(),
            $('#booktable').find('#booktit7').text(),
            $('#booktable').find('#booktit8').text(),
            $('#booktable').find('#booktit9').text(),
        ];
        for (var i = 0; i < 10; i++) {
            for (var j = i + 1; j < 10; j++){
                if (sort[i] > sort[j]) {
                    var temp = sort[i];
                    var temp2 = btits[i];
                    sort[i] = sort[j];
                    btits[i] = btits[j];
                    sort[j] = temp;
                    btits[j] = temp2;
                }
            }
        }
        var modtxt = ''
        for (var ii = 9; ii > 4; ii--) {
            modtxt += '<p><b>' + btits[ii] + '</b>: ' + sort[ii] + ' likes</p>';
        }
        $('#inmodal').html(modtxt);
    });
    $('#booktable').on('click','#btn9',function() {
        var sort = [
            parseInt($('#booktable').find('#like0').text()),
            parseInt($('#booktable').find('#like1').text()),
            parseInt($('#booktable').find('#like2').text()),
            parseInt($('#booktable').find('#like3').text()),
            parseInt($('#booktable').find('#like4').text()),
            parseInt($('#booktable').find('#like5').text()),
            parseInt($('#booktable').find('#like6').text()),
            parseInt($('#booktable').find('#like7').text()),
            parseInt($('#booktable').find('#like8').text()),
            parseInt($('#booktable').find('#like9').text())
        ];
        var btits = [
            $('#booktable').find('#booktit0').text(),
            $('#booktable').find('#booktit1').text(),
            $('#booktable').find('#booktit2').text(),
            $('#booktable').find('#booktit3').text(),
            $('#booktable').find('#booktit4').text(),
            $('#booktable').find('#booktit5').text(),
            $('#booktable').find('#booktit6').text(),
            $('#booktable').find('#booktit7').text(),
            $('#booktable').find('#booktit8').text(),
            $('#booktable').find('#booktit9').text(),
        ];
        for (var i = 0; i < 10; i++) {
            for (var j = i + 1; j < 10; j++){
                if (sort[i] > sort[j]) {
                    var temp = sort[i];
                    var temp2 = btits[i];
                    sort[i] = sort[j];
                    btits[i] = btits[j];
                    sort[j] = temp;
                    btits[j] = temp2;
                }
            }
        }
        var modtxt = ''
        for (var ii = 9; ii > 4; ii--) {
            modtxt += '<p><b>' + btits[ii] + '</b>: ' + sort[ii] + ' likes</p>';
        }
        $('#inmodal').html(modtxt);
    });
})