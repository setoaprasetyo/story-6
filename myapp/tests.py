from myapp.models import StatUpdate
from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from django.test import LiveServerTestCase
from django.db import models
import time
from selenium import webdriver
import unittest


class StatTestCase(TestCase):
    def setUp(self):
        StatUpdate.objects.create(status="I'm OK")
        StatUpdate.objects.create(status="I'm not OK")

    def test_new_activity(self):
        new_activity = StatUpdate.objects.create(published=models.DateTimeField(auto_now_add = True),status="I'm OK")
        count_activity = StatUpdate.objects.all().count()
        self.assertEqual(count_activity,3)
    def test_can_save_post(self):
        response = self.client.post('/',data={'published': '2020-10-10T08:30', 'status': "I'm not OK"})
        count_activity = StatUpdate.objects.all().count()
        self.assertEqual(count_activity, 3)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/updated/')
        new_response = self.client.get('/')
        html_response = new_response.content.decode('utf8')
        self.assertIn("OK", html_response)

    def test_lab6_profile_contain_name(self):
        title = 'HELLO, HOW ARE YOU?'
        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(title, html_response)
    def test_lab6_profile_contain_name(self):
        title = 'Select Theme:'
        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(title, html_response)
    def test_lab6_profile_contain_name(self):
        title = 'Current activities'
        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(title, html_response)
    def test_lab6_profile_contain_name(self):
        title = 'Organizational experience'
        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(title, html_response)
    def test_lab6_profile_contain_name(self):
        title = 'Committee experience'
        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(title, html_response)
    def test_lab6_profile_contain_name(self):
        title = 'Achievements'
        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(title, html_response)
    def test_lab6_updated_contain_name(self):
        title = 'You Did It! You Submitted A Status!'
        response = Client().get('/updated/')
        html_response = response.content.decode('utf8')
        self.assertIn(title, html_response)
    def test_lab6_updated_contain_name_boogaloo(self):
        title = 'What An Accomplishment!'
        response = Client().get('/updated/')
        html_response = response.content.decode('utf8')
        self.assertIn(title, html_response)
    def test_lab6_updated_contain_name_trinity(self):
        title = 'Return'
        response = Client().get('/updated/')
        html_response = response.content.decode('utf8')
        self.assertIn(title, html_response)
    def test_lab6_profile_contain_status(self):
        title = 'Status'
        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(title, html_response)
    def test_lab6_profile_contain_submit(self):
        title = 'Submit'
        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(title, html_response)
    def test_lab6_using_updated_func(self):
        found = resolve('/updated/')
        self.assertNotEqual(found.func, 'updated')
    def test_lab6_using_index_func(self):
        found = resolve('/')
        self.assertNotEqual(found.func, 'start')
    def test_URLexists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)
    def test_URLexists(self):
        response = Client().get('/updated/')
        self.assertEqual(response.status_code,200)
    def test_rightTemplate(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')
    def test_rightTemplate(self):
        response = Client().get('/updated/')
        self.assertTemplateUsed(response, 'youdidit.html')

class NewStatusTest(unittest.TestCase):
    def setUp(self):
        self.browser = webdriver.Chrome()

    def test_can_start_a_list_and_retrieve_it_later(self):
        self.browser.get('http://127.0.0.1:8000/')
        time.sleep(5)
        search_box = self.browser.find_element_by_name('status')
        search_box.send_keys('functional test')
        search_box.submit()
        time.sleep(5)
        self.assertIn('What An Accomplishment!',self.browser.page_source)
    def test_button_press(self):
        self.browser.get('http://127.0.0.1:8000/updated/')
        time.sleep(5)
        self.browser.find_element_by_name('ret').click()
        time.sleep(5)
        self.assertIn('HELLO, HOW ARE YOU?',self.browser.page_source)
    def test_can_start_a_list_and_retrieve_it_later(self):
        self.browser.get('http://127.0.0.1:8000/')
        time.sleep(5)
        self.browser.find_element_by_id('numb2').click()
        time.sleep(5)
        self.assertIn("I've been in an art club",self.browser.page_source)
    def test_can_start_a_list_and_retrieve_it_later(self):
        self.browser.get('http://127.0.0.1:8000/')
        time.sleep(5)
        self.browser.find_element_by_id('numb1').click()
        time.sleep(5)
        self.assertIn("Currently partaking online college",self.browser.page_source)
    def test_can_start_a_list_and_retrieve_it_later(self):
        self.browser.get('http://127.0.0.1:8000/')
        time.sleep(5)
        self.browser.find_element_by_id('numb3').click()
        time.sleep(5)
        self.assertIn("Never been anything above that",self.browser.page_source)
    def test_can_start_a_list_and_retrieve_it_later(self):
        self.browser.get('http://127.0.0.1:8000/')
        time.sleep(5)
        self.browser.find_element_by_id('numb4').click()
        time.sleep(5)
        self.assertIn("never have I ever been near top 5.",self.browser.page_source)
    def test_can_start_a_list_and_retrieve_it_later(self):
        self.browser.get('http://127.0.0.1:8000/')
        time.sleep(5)
        self.browser.find_element_by_id('dw1').click()
        time.sleep(5)
        self.browser.find_element_by_id('numb2').click()
        time.sleep(5)
        self.assertIn("Currently partaking online college",self.browser.page_source)
        time.sleep(5)
        self.browser.find_element_by_id('numb1').click()
        time.sleep(5)
        self.assertIn("I've been in an art club",self.browser.page_source)
        time.sleep(5)
        self.browser.find_element_by_id('numb3').click()
        time.sleep(5)
        self.assertIn("Never been anything above that",self.browser.page_source)



if __name__ == '__main__':
    unittest.main(warnings='ignore')