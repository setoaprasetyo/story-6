from django.db import models

# Create your models here.
class StatUpdate(models.Model):
    status = models.CharField(max_length= 100)

    published = models.DateTimeField(auto_now_add = True)
    updated = models.DateTimeField(auto_now = True)

    def __str__(self):
        return "{}. {}".format(self.id, self.status)