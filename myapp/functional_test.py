from selenium import webdriver
import unittest
import time

class NewStatusTest(unittest.TestCase):
    def setUp(self):
        self.browser = webdriver.Chrome()

    def test_can_start_a_list_and_retrieve_it_later(self):
        self.browser.get('http://127.0.0.1:8000/')
        time.sleep(5)
        search_box = self.browser.find_element_by_name('status')
        search_box.send_keys('functional test')
        search_box.submit()
        time.sleep(5)
        self.assertIn('functional test',self.browser.page_source)

if __name__ == '__main__':
    unittest.main(warnings='ignore')