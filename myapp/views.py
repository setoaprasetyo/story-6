from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .models import StatUpdate
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm

def start(request):
    posts = StatUpdate.objects.all()
    num_visits = request.session.get('num_visits', 0)
    num_status = request.session.get('num_status', 0)
    request.session['num_visits'] = num_visits + 1
    context = {
        'posts':posts,
        'num_visits': num_visits,
        'num_status': num_status,
    }
    if request.method == 'POST':
        StatUpdate.objects.create(
        status = request.POST['status']
        )
        request.session['num_status'] = num_status + 1
        return HttpResponseRedirect("/")
    request.session.modified = True

    return render(request,'index.html', context)

def updated(request):
    return render(request,'youdidit.html')

def books(request):
    num_visits = request.session.get('num_visits2', 0)
    request.session['num_visits2'] = num_visits + 1
    context = {
        'num_visits2': num_visits,
    }
    request.session.modified = True
    return render(request,'books.html', context)

def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return HttpResponseRedirect('/')
    else:
        form = UserCreationForm()
    return render(request, 'signup.html', {'form': form})