"""Story6 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url
from django.urls import include, path
from django.conf import settings
from django.conf.urls.static import static
from . import views
from myapp import views as myViews

urlpatterns = [
    path(r'admin/', admin.site.urls),
    url(r'^$', myViews.start),
    url(r'updated/$', myViews.updated),
    url(r'books/$', myViews.books),
    path('',include('django.contrib.auth.urls')),
    url(r'signup/$', myViews.signup, name='signup'),
    ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

